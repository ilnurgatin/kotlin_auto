package homeWorks.homework2

/**
 * @author i.gatin
 */
fun main() {
    val simonsCat = Cat()
    useRunSkill(simonsCat)
    useSwimSkill(simonsCat)
    useSwimAndRunSkill(simonsCat)

    val mufasa = Lion()
    useRunSkill(mufasa)
    useSwimSkill(mufasa)
    useSwimAndRunSkill(mufasa)

    val nemo = Salmon()
    useSwimSkill(nemo)

    val ariel = Fish()
    useSwimSkill(ariel)
}

abstract class Pet {
}

open class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and i running")
    }

    override fun swim() {
        println("I am a Cat, and i swimming")
    }
}

open class Fish : Pet(), Swimmable {
    override fun swim() {
        println("I am a Fish, and i swimming")
    }
}

class Tiger : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Tiger, and i running")
    }

    override fun swim() {
        println("I am a Tiger, and i swimming")
    }
}

class Lion : Cat(), Runnable, Swimmable {
    override fun run() {
        println("I am a Lion, and i running")
    }

    override fun swim() {
        println("I am a Lion, and i swimming")
    }
}

class Salmon : Fish(), Swimmable {
    override fun swim() {
        println("I am a Salmon, and i swimming")
    }
}

class Tuna : Fish(), Swimmable {
    override fun swim() {
        println("I am a Tuna, and i swimming")
    }
}

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

fun <E : Runnable> useRunSkill(e: E) {
    e.run()
}

fun <E : Swimmable> useSwimSkill(e: E) {
    e.swim()
}

fun <E> useSwimAndRunSkill(e: E) where E:Runnable, E:Swimmable {
    e.run()
    e.swim()
}
