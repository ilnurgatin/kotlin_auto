package homeWorks.homework2

/**
 * @author i.gatin
 */
val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiply = { a: Double, b: Double -> a * b }
val divide = { a: Double, b: Double ->
    if(b == 0.0) {
        throw IllegalArgumentException("На ноль делить нельзя")
    } else {
        a / b
    }
}

fun main() {
    calculator(getCalculationMethod("+"), 2.0, 3.0)
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 4.0, 5.5)
    calculator(getCalculationMethod("/"), 30.0, 0.0)
}

fun calculator(lambda : ((Double, Double)-> Double), a: Double, b: Double ) {
    println(lambda(a,b))
}

fun getCalculationMethod(name: String) :(Double, Double)-> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiply
        "/" -> divide
        else -> throw UnsupportedOperationException()
    }
}
