package homeWorks.homework2

import java.text.DecimalFormat

fun main() {
    val list = listOf<Double?>(13.31, 3.98, 12.0, 2.99, 9.0)
    println(DecimalFormat("#.##").format(sortAndFilter(list)))

    val list2 = listOf<Double?>(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    println(DecimalFormat("#.##").format(sortAndFilter(list2)))
}

fun sortAndFilter(list: List<Double?>): Double {
    val withoutNulls = list.filterNotNull()
    val odds = withoutNulls.filter { it.toInt() % 2 != 0 }
        .map { it / 2 }
        .filter { it < 25 }

    val evens = withoutNulls.filter { it.toInt() % 2 == 0 }
        .map { it * it }
        .filter { it < 25 }

    return (odds + evens).sortedDescending()
        .take(10)
        .sum()
}