package homeWorks.homework2


/**
 * @author i.gatin
 */
fun validateInnFunction(inn: String, function: (String) -> Boolean): Boolean {
    return function(inn)
}
fun main() {
    val inn = "707574944998"
    validateInnFunction(inn, validateInn)
}

val firstCheckMultipliers = arrayListOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
val secondCheckMultipliers = arrayListOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)

val validateInn: (String) -> Boolean = {inn: String ->
    var result: Boolean
    val innStr = inn.split("").filter { it != "-" }.filter{it.isNotBlank()}

    val innLength = innStr.size

    if (innLength != 12 || !innStr.joinToString("").all { it.isDigit() }) {
        result = false
    } else {
        val innAsListOfNumbers = innStr.map { it.toInt() }

        /*
        Вычислить 1-ю контрольную цифру:
        Вычислить сумму произведений цифр ИНН (с 1-й по 10-ю)
        на следующие коэффициенты — 7, 2, 4, 10, 3, 5, 9, 4, 6, 8
        (т.е. 7 * ИНН[1] + 2 * ИНН[2] + ...).
         */
        val firstTenNumbers: MutableList<Int> = innAsListOfNumbers.take(10).toMutableList()
        firstTenNumbers.forEachIndexed { index, i ->
            firstTenNumbers[index] = i * firstCheckMultipliers[index]
        }
        val firstSum = firstTenNumbers.sum()

        /*
        Вычислить младший разряд остатка от деления полученной суммы на 11.
         */
        val firstControlNumber = (firstSum % 11) % 10

        /*
        Вычислить 2-ю контрольную цифру:
        Вычислить сумму произведений цифр ИНН (с 1-й по 11-ю)
        на следующие коэффициенты — 3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8
        (т.е. 3 * ИНН[1] + 7 * ИНН[2] + ...).
         */
        val firstElevenNumbers: MutableList<Int> = innAsListOfNumbers.take(11).toMutableList()
        firstElevenNumbers.forEachIndexed { index, i ->
            firstElevenNumbers[index] = i * secondCheckMultipliers[index]
        }
        val secondSum = firstElevenNumbers.sum()

        /*
        Вычислить младший разряд остатка от деления полученной суммы на 11.
         */
        val secondControlNumber = (secondSum % 11) % 10

        /*
        Сравнить 1-ю контрольную цифру с 11-й цифрой ИНН и сравнить 2-ю контрольную цифру с 12-й цифрой ИНН.
        Если они равны, то ИНН верный.
         */
        result = !(firstControlNumber != innAsListOfNumbers[10] || secondControlNumber != innAsListOfNumbers[11])
    }
    result
}

