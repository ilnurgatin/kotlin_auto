package homeWorks.homework1

fun main() {
    val list = mutableListOf<Int>(1, 4, 9, 16, 25)
    val newList = list.square()
    println(newList)
}

fun MutableList<Int>.square(): List<Int> {
    return this.map { it * it  }
}