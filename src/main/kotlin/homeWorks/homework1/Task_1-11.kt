package homeWorks.homework1

import homeWorks.homework1.Counter.Companion.counter

fun main() {
    counter()
    counter()
    counter()
    counter()
}

class Counter() {
    companion object {
        var count = 0

        fun counter() {
            count++
            println(String.format("Вызван counter.  Количество вызовов = %d", count))
        }
    }
}