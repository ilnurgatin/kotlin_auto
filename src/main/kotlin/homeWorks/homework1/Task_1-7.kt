package homeWorks.homework1

fun main() {
    val list = arrayOf("12", "122", "1234", "fpo")
    fu(*list)
}

fun fu(vararg params: String) {
    println(String.format("Передано %d элементов", params.size))
    println(params.joinToString(";"))
}