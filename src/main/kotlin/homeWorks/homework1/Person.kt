package homeWorks.homework1

fun main() {
    println(
        configPerson(
            firstName = "Ivan",
            lastName = "Ivanov",
            age = 18,
            dateOfBirth = "10-02-2004",
            gender = "Male",
        )
    )
}

fun configPerson(firstName: String, lastName: String,
                 age: Int, dateOfBirth: String, gender: String,
                 inn: Int? = null, snils: Int? = null): Person {
    return Person(firstName, lastName, age, dateOfBirth, gender, inn, snils)
}

class Person(val firstName: String, val lastName: String, var age: Int, val dateOfBirth: String,
             val gender: String, val inn: Int?, val snils: Int?) {

    override fun toString(): String {
        return "Person(name=$firstName, lastname=$lastName, age=$age, dateOfBirth=$dateOfBirth, " +
                "gender=$gender, inn=$inn, snils=$snils"
    }
}