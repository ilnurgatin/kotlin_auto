package homeWorks.homework1

fun main() {
    configPersonData(
        firstName = "Ivan",
        lastName = "Ivanov",
        middleName = "Ivanovich",
        gender = "Male",
        age = 20,
        dateOfbirth = "10-12-2004",
        inn = 34343,
        snils = 75757564,
    )
    configPersonData(
        firstName = "Ivan",
        lastName = "Ivanov",
        middleName = "Ivanovich",
        gender = "Male",
        age = 20,
        dateOfbirth = "10-12-2004",
    )
    configPersonData(
        middleName = "Ivanovich",
        gender = "Male",
        age = 20,
        dateOfbirth = "10-12-2004",
        firstName = "Ivan",
        lastName = "Ivanov",
    )
}

fun configPersonData(firstName: String, lastName: String, middleName: String, gender: String, age: Int, dateOfbirth: String, inn: Int? = null, snils: Int? = null) {
    println("firstName=$firstName, lastName=$lastName, middleName=$middleName, gender=$gender, age=$age, dateOfbirth=$dateOfbirth, inn=$inn, snils = $snils")
}