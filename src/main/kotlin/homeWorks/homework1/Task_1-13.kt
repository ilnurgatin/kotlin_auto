package homeWorks.homework1

import java.time.LocalDate
import java.util.logging.Handler
import kotlin.math.roundToInt

fun main() {
    customCast("Privet")
    customCast(145)
    customCast(145.0)
    customCast(145.2817812)
    customCast(LocalDate.of(1990,1,1))
    customCast(Handler::class)
}

fun customCast(any: Any?) {
    if (any == null) {
        println("Объект равне null")
        return
    }
    when(any) {
        is String -> {
            println(String.format(
                    "Я получил тип String = ‘%s’, ее длина равна %d символ", any, any.length
                )
            )
        }
        is Int -> {
            val qval = any * any
            println(String.format(
                    "Я получил Int = %d, его квадрат равен %d", any, qval
                )
            )
        }
        is Double -> {
            if (any.toInt().toDouble() == any) {
                val rounded = any.toInt()
                println("Я получил Double = $any, это число округляется до $rounded")
            } else {
                val rounded = (any * 100).roundToInt() / 100.0
                println("Я получил Double = $any, это число округляется до $rounded")
            }
        }
        is LocalDate -> {
            val tinkoffDate: LocalDate = LocalDate.of(2006,12,24)
            if (any < tinkoffDate ) {
                println(String.format("Я получил LocalData = %td.%tm.%ty, эта дата меньше чем дата основания Tinkoff", any, any, any))
            } else {
                println(String.format("Я получил LocalData = %td.%tm.%ty, эта дата позже или равна дате основания Tinkoff", any, any, any))
            }
        }
        else -> {
            println("Мне этот тип неизвестен(")
        }
    }
}
