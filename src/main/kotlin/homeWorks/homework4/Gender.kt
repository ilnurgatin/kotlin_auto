package homeWorks.homework4

/**
 * @author i.gatin
 */
enum class Gender(value: String) {
    MALE("Муж"),
    FEMALE("Жен")
}
