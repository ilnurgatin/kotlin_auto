package homeWorks.homework4

import com.itextpdf.kernel.font.PdfFont
import com.itextpdf.kernel.font.PdfFontFactory
import com.itextpdf.kernel.geom.PageSize
import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Table
import java.time.format.DateTimeFormatter

/**
 * @author i.gatin
 */
class PdfSaver: DocumentSaver {
    override fun save(persons: List<Person>, filePath: String) {
        val writer = PdfWriter(filePath)
        val pdfDoc = PdfDocument(writer)
        val pageSize = PageSize.A4.rotate()
        pdfDoc.defaultPageSize = pageSize
        val document = Document(pdfDoc)

        val fontPath = "src/main/kotlin/homeWorks/homework4/resources/ofont.ru_Hero.ttf"
        val font: PdfFont = PdfFontFactory.createFont(fontPath, "Identity-H", true)

        val table = Table(15).useAllAvailableWidth()
        table.setFont(font).setFontSize(8f)
        table.addHeaderCell("Имя")
        table.addHeaderCell("Фамилия")
        table.addHeaderCell("Отчество")
        table.addHeaderCell("Возраст")
        table.addHeaderCell("Пол")
        table.addHeaderCell("Дата рождения")
        table.addHeaderCell("Место рождения")
        table.addHeaderCell("Индекс")
        table.addHeaderCell("Страна")
        table.addHeaderCell("Область")
        table.addHeaderCell("Город")
        table.addHeaderCell("Улица")
        table.addHeaderCell("Дом")
        table.addHeaderCell("Квартира")
        table.addHeaderCell("ИНН")

        val dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        for (person in persons) {
            table.addCell(person.firstName)
            table.addCell(person.lastName)
            table.addCell(person.middleName)
            table.addCell(person.age.toString())
            table.addCell(person.gender)
            table.addCell(person.dateOfBirth.format(dateFormatter))
            table.addCell(person.birthPlace.city)
            table.addCell(person.address.index.toString())
            table.addCell(person.address.location.country)
            table.addCell(person.address.location.region)
            table.addCell(person.address.location.city)
            table.addCell(person.address.street)
            table.addCell(person.address.houseNumber.toString())
            table.addCell(person.address.flatNumber.toString())
            table.addCell(person.inn.toString())
        }

        document.add(table)
        document.close()
        pdfDoc.close()
        writer.close()

        println("Pdf файл создан. Путь: $filePath")
    }
}
