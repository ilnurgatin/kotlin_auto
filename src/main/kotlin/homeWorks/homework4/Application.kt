package homeWorks.homework4

/**
 * @author i.gatin
 */
class Application {
    fun start() {
        println("Введите количество мертвых душ")
        var input = readln()
        while (input.toIntOrNull() == null || input.toInt() > 30 || input.toInt() < 0) {
            println("Введенное значение должно быть числовым и находится от 1 до 30")
            input = readln()
        }
        val personCount: Int = input.toInt()
        val personFactory = PersonFactory()
        val persons = personFactory.generatePersons(personCount)

        val jsonSaver = JsonSaver()
        val pathForJson = "src/main/kotlin/homeWorks/homework4/resources/person.json"
        jsonSaver.save(persons, pathForJson)

        val pdfSaver = PdfSaver()
        val pathForPDF = "src/main/kotlin/homeWorks/homework4/resources/person.pdf"
        pdfSaver.save(persons, pathForPDF)
    }
}
