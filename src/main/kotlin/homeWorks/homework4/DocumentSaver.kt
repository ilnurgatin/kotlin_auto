package homeWorks.homework4

/**
 * @author i.gatin
 */
interface DocumentSaver {
    fun save(persons: List<Person>, filePath: String)
}
