package homeWorks.homework4

import com.google.gson.GsonBuilder
import java.io.FileOutputStream
import java.time.LocalDate

/**
 * @author i.gatin
 */
class JsonSaver: DocumentSaver {
    override fun save(persons: List<Person>, filePath: String) {
        val customGson = GsonBuilder()
            .registerTypeAdapter(LocalDate::class.java, LocalDateTypeAdapter())
            .setPrettyPrinting()
            .create()
        val jsonOutput = customGson.toJson(persons)
        FileOutputStream(filePath).bufferedWriter().use { it.write(jsonOutput) }

        println("JSON файл создан. Путь: $filePath")
    }
}
