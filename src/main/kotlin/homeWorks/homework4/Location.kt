package homeWorks.homework4

/**
 * @author i.gatin
 */
data class Location(
    val country: String,
    val region: String,
    val city: String,
    )
