package homeWorks.homework4

/**
 * @author i.gatin
 */
data class Address(
    val location: Location,
    var index: Int,
    var street: String,
    var houseNumber: Int,
    var flatNumber: Int? = null,
    )
