package homeWorks.homework4


import homeWorks.homework2.validateInn
import java.time.LocalDate
import kotlin.random.Random

/**
 * @author i.gatin
 */
class PersonFactory {

    fun generatePersons(personsCount: Int): List<Person> {
        if (personsCount !in 1..30) throw IllegalArgumentException("Persons count must be between 1 and 30")

        return List(size = personsCount) { _ ->
            personGenerator()
        }
    }

    private fun personGenerator(): Person {
        val gender = genderGenerator()
        return if (gender == Gender.MALE) maleGenerator() else femaleGenerator()
    }
    
    private fun maleGenerator(): Person {
        return Person(
            firstName = selectRandomElement(DataBase.maleNames),
            lastName = selectRandomElement(DataBase.lastNames),
            middleName = selectRandomElement(DataBase.maleMiddleNames),
            dateOfBirth = dateGenerator(),
            gender = "Муж",
            birthPlace = locationGenerator()
        ).also {
            it.inn = innGenerator()
            it.address = addressGenerator()
        }
    }

    private fun femaleGenerator(): Person {
        return Person(
            firstName = selectRandomElement(DataBase.femaleNames),
            lastName = selectRandomElement(DataBase.lastNames),
            middleName = selectRandomElement(DataBase.femaleMiddleNames),
            dateOfBirth = dateGenerator(),
            gender = "Жен",
            birthPlace = locationGenerator()
        ).also {
            it.address = addressGenerator()
            it.inn = innGenerator()
        }
    }

    private fun innGenerator(): Long {
        var randomInn = Random.nextLong(707500000008, 707599999998)
        while (!isValidInn(randomInn.toString(), validateInn)) {
            randomInn = Random.nextLong(707500000008, 707599999998)
        }
        return randomInn
    }

    private fun dateGenerator(): LocalDate {
        val minDay = LocalDate.EPOCH.toEpochDay()
        val maxDay = LocalDate.now().toEpochDay()
        val randomDay = Random.nextLong(minDay,maxDay)
        return LocalDate.ofEpochDay(randomDay)
    }

    private fun locationGenerator(): Location {
        return Location(
            country = selectRandomElement(DataBase.countries),
            region = selectRandomElement(DataBase.regions),
            city = selectRandomElement(DataBase.cities)
        )
    }

    private fun genderGenerator(): Gender {
        return Gender.entries.toTypedArray().random()
    }

    private fun addressGenerator(): Address {
        return Address(
            location = locationGenerator(),
            index = Random.nextInt(385271, 468321),
            street = selectRandomElement(DataBase.streetNames),
            houseNumber = Random.nextInt(1, 250),
            flatNumber = Random.nextInt(1, 250),
        )
    }

    private fun selectRandomElement(list: List<String>): String {
        val randomValue = Random.nextInt(0, list.size)
        return list[randomValue]
    }

    private fun isValidInn(inn: String, function: (String) -> Boolean): Boolean {
        return function(inn)
    }
}
