package homeWorks.homework4

import java.time.LocalDate
import java.time.Period

/**
 * @author i.gatin
 */
data class Person(
    val firstName: String,
    val lastName: String,
    val middleName: String? = null,
    val dateOfBirth: LocalDate,
    val gender: String,
    val birthPlace: Location,
    ) {
    var age = convertBirthDateToAgeUtils(dateOfBirth)
    var inn: Long? = null
    lateinit var address: Address
}

private fun convertBirthDateToAgeUtils(date: LocalDate): Int {
    return Period.between(date, LocalDate.now()).years
}
