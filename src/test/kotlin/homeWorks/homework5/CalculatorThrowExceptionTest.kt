package homeWorks.homework5

import homeWorks.homework2.calculator
import homeWorks.homework2.divide
import homeWorks.homework2.getCalculationMethod
import org.junit.Assert.*
import org.junit.Ignore
import org.junit.Test

/**
 * @author i.gatin
 */
class CalculatorThrowExceptionTest {

    @Test
    fun invalidCalculationMethodThrowExceptionTest() {
        assertThrows(UnsupportedOperationException::class.java) {
            calculator(getCalculationMethod("="), 1.0, 1.0)
        }
    }

    @Test
    fun divideByZeroThrowExceptionTest() {
        assertThrows(IllegalArgumentException::class.java) {
            calculator(divide, 2.0, 0.0)
        }
    }

    @Test
    @Ignore("Проверяется в параметризованном тесте класса CalculatorValidMethodsTest")
    fun calculatorValidMethodTest() {
        assertEquals(Unit, calculator(getCalculationMethod("+"), 2.0, 2.0))
    }
}
