package homeWorks.homework5

import homeWorks.homework2.calculator
import homeWorks.homework2.getCalculationMethod
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized



/**
 * @author i.gatin
 */
@RunWith(Parameterized::class)
class CalculatorValidMethodsTest(private val method: String) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data(): List<String> {
            return listOf("+", "-", "*", "/")
        }
    }

    @Test
    fun calculatorMethodTest() {
        assertEquals(Unit, calculator(getCalculationMethod(method), 2.0, 2.0))
    }

}

